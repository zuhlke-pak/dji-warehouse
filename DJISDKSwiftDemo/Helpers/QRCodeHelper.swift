//
//  QRCodeHelper.swift
//  DJISDKSwiftDemo
//
//  Created by Brian Chung on 25/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//
import UIKit

final class QRCodeHelper {
    static func detectQRCode(_ image: UIImage) -> [CIQRCodeFeature]? {
        guard let ciImage = CIImage.init(image: image) else {
            return nil
        }
        var options: [String: Any]
        let context = CIContext()
        options = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let qrDetector = CIDetector(ofType: CIDetectorTypeQRCode, context: context, options: options)
        if ciImage.properties.keys.contains((kCGImagePropertyOrientation as String)) {
            options = [CIDetectorImageOrientation: ciImage.properties[(kCGImagePropertyOrientation as String)] ?? 1]
        } else {
            options = [CIDetectorImageOrientation: 1]
        }
        let features = qrDetector?.features(in: ciImage, options: options)
        let qrFeatures = features?.compactMap { $0 as? CIQRCodeFeature }
        return qrFeatures
    }
}
