//
//  DJIVideoPreviewerHelper.swift
//  DJISDKSwiftDemo
//
//  Created by Brian Chung on 23/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//

import Foundation
import DJISDK
import DJIWidget

extension DJIVideoPreviewer {
    func setupVideoPreviewer(previewViewer: UIView, listener: DJIVideoFeedListener) {
        DJIVideoPreviewer.instance()?.setView(previewViewer)

        guard let product = DJISDKManager.product() else {
            debugPrint("Missing product")
            return
        }

        if product.isSecondaryVideoFeedModel() {
            DJISDKManager.videoFeeder()?.secondaryVideoFeed.add(listener, with: nil)
        } else {
            DJISDKManager.videoFeeder()?.primaryVideoFeed.add(listener, with: nil)
        }

        DJIVideoPreviewer.instance()?.start()
    }

    func removeVideoPreviewer(listener: DJIVideoFeedListener) {
        DJIVideoPreviewer.instance()?.unSetView()

        guard let product = DJISDKManager.product() else {
            debugPrint("Missing product")
            return
        }

        if product.isSecondaryVideoFeedModel() {
            DJISDKManager.videoFeeder()?.secondaryVideoFeed.remove(listener)
        } else {
            DJISDKManager.videoFeeder()?.primaryVideoFeed.remove(listener)
        }
    }
}
