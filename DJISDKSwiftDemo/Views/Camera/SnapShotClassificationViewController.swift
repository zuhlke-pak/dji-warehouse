//
//  SnapShotClassificationViewController.swift
//  DJISDKSwiftDemo
//
//  Created by Pak Wai Lau on 25/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import Vision
import ImageIO
import DJISDK
import DJIWidget

class SnapShotClassificationViewController: DroneCameraViewController {
    var classificationTimer: Timer?
    @IBOutlet weak var classificationLabel: UILabel!
    /// - Tag: MLModelSetup
    lazy var classificationRequest: VNCoreMLRequest = {
        do {
            /*
             Use the Swift class `MobileNet` Core ML generates from the model.
             To use a different Core ML classifier model, add it to the project
             and replace `MobileNet` with that model's generated Swift class.
             */
            let model = try VNCoreMLModel(for: Proximity().model)
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                self?.processClassifications(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
    }()

    override class func storyboardIdentifier() -> String {
        return "SnapShotClassificationViewController"
    }
    
    /// Updates the UI with the results of the classification.
    /// - Tag: ProcessClassifications
    func processClassifications(for request: VNRequest, error: Error?) {
        
        DispatchQueue.main.async { [weak self] in
            guard let classifications = request.results as? [VNClassificationObservation]
            else {
                self?.classificationLabel.text = "Unable to classify image.\n\(error?.localizedDescription ?? "")"
                return
            }
            // The `results` will always be `VNClassificationObservation`s, as specified by the Core ML model in this project.
//            guard let classifications = results as? [VNClassificationObservation]
            
            if classifications.isEmpty {
                self?.classificationLabel.text = "Nothing recognized."
            } else {
                // Display top classifications ranked by confidence in the UI.
                let topClassifications = classifications.prefix(2)
                let descriptions = topClassifications.map { classification in
                    // Formats the classification for display; e.g. "(0.37) cliff, drop, drop-off".
                    return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
                }
                self?.classificationLabel.text = "Classification:\n" + descriptions.joined(separator: "\n")
            }
        }
    }
    
    /// - Tag: PerformRequests
    func updateClassifications(for image: UIImage) {
        classificationLabel.text = "Classifying..."
        
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(Int(image.imageOrientation.rawValue))) else {return debugPrint("cannot get orientation")}
        guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
            guard let classificationRequest = self?.classificationRequest else {
                return
            }
            do {
                try handler.perform([classificationRequest])
            } catch {
                /*
                 This handler catches general image processing errors. The `classificationRequest`'s
                 completion handler `processClassifications(_:error:)` catches errors specific
                 to processing that request.
                 */
                print("Failed to perform classification.\n\(error.localizedDescription)")
            }
        }
    }

    @objc
    func classifySnapshot() {
        DJIVideoPreviewer.instance()?.snapshotPreview({ [weak self] (previewImage) in
            guard let previewImage = previewImage else {
                debugPrint("[Drone] missing preview image")
                return
            }
            
            self?.updateClassifications(for: previewImage)
        })
    }
    
    override func setupVideoPreview(){
        super.setupVideoPreview()
        classificationTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(classifySnapshot), userInfo: nil, repeats: true)
    }
    
    override func stopVideoPreview(){
        super.stopVideoPreview()
        classificationTimer?.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopQRCodeChecker()
    }
}
