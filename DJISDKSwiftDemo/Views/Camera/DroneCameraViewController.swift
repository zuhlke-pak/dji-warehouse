//
//  DroneCameraViewController.swift
//  DJISDKSwiftDemo
//
//  Created by Brian Chung on 22/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//

import Foundation
import UIKit
import DJISDK
import DJIWidget
import CoreML
import Vision
import ImageIO

class DroneCameraViewController: BaseViewController {
    @IBOutlet var previewView: UIView!
    @IBOutlet var qrStatusLabel: UILabel!
    @IBOutlet var snapshotImageView: UIImageView!
    fileprivate var previewImages = [UIImage]()
    fileprivate var isSetupCompleted: Bool = false
    fileprivate var isSnapShotMode: Bool = false {
        didSet {
            if isSnapShotMode {
                self.view.bringSubview(toFront: snapshotImageView)
            } else {
                self.view.bringSubview(toFront: previewView)
            }
            self.view.bringSubview(toFront: qrStatusLabel)
        }
    }
    fileprivate var cameraButtonItem: UIBarButtonItem!
    fileprivate var timer: Timer?
    fileprivate var storedQRFeatures = [String: CIQRCodeFeature]()

    override class func storyboardIdentifier() -> String {
        return "DroneCameraViewController"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupVideoPreview()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopVideoPreview()
    }

    @objc
    private func captureButtonDidTap(_ sender: UIBarButtonItem) {
        shootPhoto()
    }

    @objc
    private func snapShotButtonDidTap(_ sender: UIBarButtonItem) {
        isSnapShotMode = !isSnapShotMode
    }

    @objc
    private func previewImageButtonDidTap(_ sender: UIBarButtonItem) {
        let qrCodeFeatures = Array(storedQRFeatures.values)
        presentQRCodeList(qrCodeFeatures: qrCodeFeatures)
    }

    private func setupUI() {
        self.cameraButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(captureButtonDidTap(_:)))
        let previewImageButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(previewImageButtonDidTap(_:)))
        let snapShotModeButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(snapShotButtonDidTap(_:)))
        self.navigationItem.rightBarButtonItems = [cameraButtonItem, previewImageButtonItem, snapShotModeButtonItem]
    }

     func setupVideoPreview() {
        guard !isSetupCompleted else {
            return
        }
        DJIVideoPreviewer.instance()?.setupVideoPreviewer(
            previewViewer: self.previewView,
            listener: self
        )

        startQRCodeChecker()

        NotificationCenter.default.addObserver(self, selector: #selector(onDidProductConnected(_:)), name: .didProductConnected, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onDidProductDisConnected(_:)), name: .didProductDisConnected, object: nil)

        isSetupCompleted = true
    }

     func stopVideoPreview() {
        guard isSetupCompleted else {
            return
        }
        if let camera = DJISDKManager.product()?.fetchCamera() {
            camera.delegate = nil
        }

        stopQRCodeChecker()
        DJIVideoPreviewer.instance()?.removeVideoPreviewer(listener: self)
        isSetupCompleted = false
    }

     func shootPhoto() {
        guard let camera = DJISDKManager.product()?.fetchCamera() else {
            return
        }
        camera.delegate = self
        camera.setShootPhotoMode(DJICameraShootPhotoMode.single) { [weak self] (error) in
            guard error == nil else {
                self?.showAlert(title: nil, message: "Fail to set camera mode to single mode")
                return
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
                camera.startShootPhoto(completion: { [weak self] (error) in
                    guard let error = error else {
                        return
                    }
                    self?.showAlert(title: "Take Photo Error", message: error.localizedDescription)
                })
            })
        }
    }

    private func startQRCodeChecker() {
        guard self.timer == nil else {
            return
        }
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(checkQRCode), userInfo: nil, repeats: true)
    }

     func stopQRCodeChecker() {
        self.timer?.invalidate()
        self.timer = nil
    }

    @objc
    private func checkQRCode() {
        DispatchQueue.main.async {
            DJIVideoPreviewer.instance()?.snapshotPreview({ [weak self] (previewImage) in
                guard let previewImage = previewImage else {
                    Logger.log(message: "Missing preview image", event: .debug)
                    return
                }
                self?.snapshotImageView.image = previewImage
                if let qrFeatures = QRCodeHelper.detectQRCode(previewImage),
                    !qrFeatures.isEmpty {
                    var concatedQrMessages = ""
                    for case let qrFeature in qrFeatures {
                        self?.updateStoredQRFeatureIfNeeded(qrFeature)
                        if let message = qrFeature.messageString {
                            concatedQrMessages += "(\(message))"
                        }
                    }
                    self?.qrStatusLabel.text = concatedQrMessages
                } else {
                    self?.qrStatusLabel.text = ""
                }
            })
        }
    }

    private func updateStoredQRFeatureIfNeeded(_ qrCodeFeature: CIQRCodeFeature) {
        guard let message = qrCodeFeature.messageString,
            storedQRFeatures[message] == nil else {
            return
        }
        storedQRFeatures[message] = qrCodeFeature
    }

    @objc
    private func onDidProductConnected(_ notification: Notification) {
        setupVideoPreview()
    }

    @objc
    private func onDidProductDisConnected(_ notification: Notification) {
        stopVideoPreview()
    }
}

extension DroneCameraViewController: DJIVideoFeedListener {
    func videoFeed(_ videoFeed: DJIVideoFeed, didUpdateVideoData videoData: Data) {
        let videoPreviewer = DJIVideoPreviewer.instance()
        let tmpVideoData = UnsafeMutablePointer<UInt8>.allocate(capacity: videoData.count)
        videoData.copyBytes(to: tmpVideoData, count: videoData.count)
        videoPreviewer?.push(tmpVideoData, length: Int32(videoData.count))
    }
}

extension DroneCameraViewController: DJICameraDelegate {
    func camera(_ camera: DJICamera, didGenerateNewMediaFile newMedia: DJIMediaFile) {
        // TODO: Implement later
    }
}
