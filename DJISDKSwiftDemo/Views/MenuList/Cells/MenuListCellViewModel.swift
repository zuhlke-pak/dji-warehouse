//
//  MenuListCellViewModel.swift
//  DJI-Warehouse
//
//  Created by Brian Chung on 31/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//

import Foundation

struct MenuListCellViewModel {
    enum MenuType {
        case autoFly
        case qrCamera
        case mlCamera
        case info
        case mission
        case testQr

        func toStoryboardIdentifier() -> String {
            switch self {
            case .autoFly:
                return AutoFlyViewController.storyboardIdentifier()
            case .qrCamera:
                return DroneCameraViewController.storyboardIdentifier()
            case .mlCamera:
                return SnapShotClassificationViewController.storyboardIdentifier()
            case .testQr:
                return TestQRViewController.storyboardIdentifier()
            case .info:
                return KeyedInterfaceViewController.storyboardIdentifier()
            case .mission:
                return TimelineMissionViewController.storyboardIdentifier()
            }
        }
    }

    var title: String
    var type: MenuType
}
