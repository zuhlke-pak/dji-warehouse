//
//  TestViewController.swift
//  DJISDKSwiftDemo
//
//  Created by Brian Chung on 28/1/2019.
//  Copyright © 2019 DJI. All rights reserved.
//

import UIKit

final class TestQRViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let features = QRCodeHelper.detectQRCode(imageView.image!) {
            Logger.log(message: "num of features count:\(features.count)", event: .debug)
            _ = features.map { debugPrint("[PK] feature message:\($0.messageString ?? "")") }
            Logger.log(message: "bottomLeft: \(features[0].bottomLeft), bottomLeft: \(features[0].topLeft)", event: .debug)
            Logger.log(message: "angel:\(features[0].bottomLeft.angle(to: features[0].topLeft))", event: .debug)
        }
    }

    override class func storyboardIdentifier() -> String {
        return "TestQRViewController"
    }
}
